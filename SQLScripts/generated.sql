INSERT INTO Object VALUES ('eightnine','14','Testament - The Gathering','awesome band, awesome CD','null','Music','11:33:44','24.05.2014');
INSERT INTO Object VALUES ('qusdu','3','Fujitsu S3','Flat Monitor','null','PC Parts','01:01:00','11.05.2014');
INSERT INTO Object VALUES ('qusdu','4','Folding Chair','nice old folding chair, heavily used','null','Antiques','01:15:45','11.05.2014');
INSERT INTO Object VALUES ('qusdu','5','Unique Teapot','Old Teapot, it belonged to my aunt','null','Antiques','01:23:21','11.05.2014');
INSERT INTO Object VALUES ('qusdu','6','Nice Tarp','old tarp. No idea where it came from.','null','Antiques','01:24:33','11.05.2014');
INSERT INTO Object VALUES ('qusdu','7','Nokia 3210','it still works. Magic.','null','Antiques','01:27:22','11.05.2014');
INSERT INTO Object VALUES ('qusdu','8','Beautiful Lamp','Old lamp, it belonged to my aunt. Im not robbing my aunt, swear.','null','Antiques','01.29:11','11.05.2014');
INSERT INTO Object VALUES ('qusdu','9','Used Sheets','Barely used, I think. I might be robbing my aunt for these.','null','Antiques','01:33:55','11.05.2014');
INSERT INTO Object VALUES ('qusdu','10','Nice Painting','old painting I found somewhere. Not at my aunts.','null','Antiques','01:45:03','11.05.2014');
INSERT INTO Object VALUES ('qusdu','11','Stamp Collection','huge collection of antique stamps','null','Antiques','01:48:34','11.05.2014');



INSERT INTO Object VALUES ('marym','0','Party Dress','nice dress for parties','null','Clothes','09:57:12','23.3.2014');
INSERT INTO Object VALUES ('marym','1','Party Shoes','nice shoes for parties','null','Clothes','10:02:33','23.03.2014');
INSERT INTO Object VALUES ('marym','2','cargo Pants','not sure why I have these','null','Clothes','10:05:23','23.03.2014');
INSERT INTO Object VALUES ('marym','3','Samsung Galaxy Chat','it still works, kinda. Cant access SD anymore','null','Cellphones','10:11:11','23.03.2014');
INSERT INTO Object VALUES ('marym','4','Barbie Doll','selling my childhood feels right','null','Toys','11:23:45','23.03.2014');
INSERT INTO Object VALUES ('marym','5','Ken Doll','cant sell barbie without her partner','null','Toys','11:25','23.03.2014');
INSERT INTO Object VALUES ('marym','6','Digimon Dolls','Almost all Digimon dolls','null','Toys','11:27:23','23.03.2014');
INSERT INTO Object VALUES ('marym','7','The Usual Suspects DVD','DVD of one of the best movies ever','null','Movies','17:23:12','30.05.2014');
INSERT INTO Object VALUES ('marym','8','Independence Day DVD','DVD, still new','null','Movies','17:25:11','30.05.2014');
INSERT INTO Object VALUES ('marym','9','Catch 22','Good book, I have the kindle version now so I dont need it anymore','null','Books','11:22:33','14.06.2014');
INSERT INTO Object VALUES ('marym','10','Dune','probably the best book ever','null','Books','11:25:31','14.06.2014');
INSERT INTO Object VALUES ('marym','11','Goedel, Escher, Bach','GEB, EGB, a masterpiece','null','Books','11:33:21','14.06.2014');




INSERT INTO Auction VALUES ('eightnine','0','12.03.2014','12.04.2014','1500','140','null');
INSERT INTO Auction VALUES ('eightnine','1','12.03.2014','12.04.2014','890','140','null');
INSERT INTO Auction VALUES ('eightnine','2','12.03.2014','12.04.2014','240','30','null');
INSERT INTO Auction VALUES ('eightnine','3','140','12.04.2014','140','10','null');
INSERT INTO Auction VALUES ('eightnine','4','12.03.2014','12.04.2014','240','10','null');
INSERT INTO Auction VALUES ('eightnine','5','12.03.2014','12.04.2014','30','10','null');
INSERT INTO Auction VALUES ('eightnine','6','12.03.2014','12.04.2014','12','0','null');
INSERT INTO Auction VALUES ('eightnine','7','23.05.2014','23.06.2014','10','0','null');
INSERT INTO Auction VALUES ('eightnine','8','23.05.2014','23.06.2014','10','0','null');
INSERT INTO Auction VALUES ('eightnine','9','23.05.2014','23.06.2014','10','0','null');
INSERT INTO Auction VALUES ('eightnine','10','23.05.2014','23.06.2014','5','0','null');
INSERT INTO Auction VALUES ('eightnine','11','23.05.2014','23.06.2014','120','10','null');
INSERT INTO Auction VALUES ('eightnine','12','23.05.2014','23.06.2014','14','0','null');
INSERT INTO Auction VALUES ('eightnine','13','24.05.2014','24.06.2014','9450','0','null');
INSERT INTO Auction VALUES ('eightnine','14','24.05.2014','24.06.2014','10','0','null');


INSERT INTO Auction VALUES ('qusdu','0','10.02.2014','10.03.2014','40','10','null');
INSERT INTO Auction VALUES ('qusdu','1','10.02.2014','10.03.2014','20','0','null');
INSERT INTO Auction VALUES ('qusdu','2','10.02.2014','10.03.2014','30','0','null');
INSERT INTO Auction VALUES ('qusdu','3','11.05.2014','11.06.2014','200','20','null');
INSERT INTO Auction VALUES ('qusdu','4','11.05.2014','11.06.2014','30','0','null');
INSERT INTO Auction VALUES ('qusdu','5','11.05.2014','11.06.2014','50','0','null');
INSERT INTO Auction VALUES ('qusdu','6','11.05.2014','11.06.2014','200','30','null');
INSERT INTO Auction VALUES ('qusdu','7','11.05.2014','11.06.2014','10','0','null');
INSERT INTO Auction VALUES ('qusdu','8','11.05.2014','11.06.2014','80','20','null');
INSERT INTO Auction VALUES ('qusdu','9','11.05.2014','11.06.2014','10','0','null');
INSERT INTO Auction VALUES ('qusdu','10','11.05.2014','11.06.2014','300','10','null');
INSERT INTO Auction VALUES ('qusdu','11','11.05.2014','11.06.2014','80','10','null');
INSERT INTO Auction VALUES ('marym','0','23.03.2014','23.04.2014','120','10','null');
INSERT INTO Auction VALUES ('marym','1','23.03.2014','23.04.2014','80','10','null');
INSERT INTO Auction VALUES ('marym','2','23.03.2014','23.04.2014','30','0','null');
INSERT INTO Auction VALUES ('marym','3','23.03.2014','23.04.2014','80','0','null');
INSERT INTO Auction VALUES ('marym','4','23.03.2014','23.04.2014','10','0','null');
INSERT INTO Auction VALUES ('marym','5','23.03.2014','23.04.2014','10','0','null');
INSERT INTO Auction VALUES ('marym','6','23.03.2014','23.04.2014','20','0','null');
INSERT INTO Auction VALUES ('marym','7','30.05.2014','30.06.2014','20','0','null');
INSERT INTO Auction VALUES ('marym','8','30.05.2014','30.06.2014','10','0','null');
INSERT INTO Auction VALUES ('marym','9','14.06.2014','14.07.2014','10','0','null');
INSERT INTO Auction VALUES ('marym','10','14.06.2014','14.07.2014','10','0','null');
INSERT INTO Auction VALUES ('marym','11','14.06.2014','14.07.2014','10','0','null');


INSERT INTO Offer VALUES ('qusdu','eightnine','0','2014-03-12','2014-03-12 13:04:11','1550');
INSERT INTO Offer VALUES ('marym','eightnine','0','2014-03-12','2014-03-14 11:00:23','1680');
INSERT INTO Offer VALUES ('taccarlo','eightnine','0','2014-03-12','2014-03-14 12:02:03','1720');
INSERT INTO Offer VALUES ('vincy','eightnine','0','2014-03-12','2014-03-22','2000');
INSERT INTO Offer VALUES ('saraj','eightnine','0','2014-03-12','2014-03-30','2500');
INSERT INTO Offer VALUES ('mennyrally','eightnine','1','2014-03-12','2014-03-13 13:03:00','900');


INSERT INTO Offer VALUES ('vincy','eightnine','1','2014-03-12','2014-03-14 22:02:11','1000');
INSERT INTO Offer VALUES ('taccarlo','eightnine','1','2014-03-12','2014-03-14 23:00:11','1001');
INSERT INTO Offer VALUES ('castle','eightnine','1','2014-03-12','2014-03-16 10:03:24','1300');
INSERT INTO Offer VALUES ('marym','eightnine','1','2014-03-12','2014-03-24 13:04:12','1650');
INSERT INTO Offer VALUES ('mennyrally','eightnine','2','2014-03-12','2014-03-13 23:02:11','300');
INSERT INTO Offer VALUES ('qusdu','eightnine','2','2014-03-12','2014-03-14 11:11:10','350');
INSERT INTO Offer VALUES ('peterbigface','eightnine','2','2014-03-12','2014-03-14 14:03:12','500');
INSERT INTO Offer VALUES ('eightnine','qusdu','0','2014-02-10','2014-02-11 15:02:11','50');
INSERT INTO Offer VALUES ('marym','qusdu','0','2014-02-10','2014-02-11 16:03:12','60');
INSERT INTO Offer VALUES ('taccarlo','qusdu','0','2014-02-10','2014-02-12 09:11:34','80');
INSERT INTO Offer VALUES ('eightnine','qusdu','1','2014-02-10','2014-02-11 13:04:02','40');
