INSERT INTO Users VALUES ( 'nowinner', 'No', 'Winner', 'bucvrh8ofghrp7otj6p866d5k3', 'bfb076d2546d763ba9c2ad2be791e0859ec409116c016d47c2df90b0e6698b5d', 'fake', 'Fake', 'nope@gmail.com', null );
INSERT INTO Users VALUES ( 'eightnine', 'Nick', 'Stone', 'bucvrh8ofghrp7otj6p866d5k3', 'bfb076d2546d763ba9c2ad2be791e0859ec409116c016d47c2df90b0e6698b5d', 'home', 'Verona', 'eightnine@gmail.com', 'MRSNCL83C25B296S' );
INSERT INTO Users VALUES ( 'qusdu', 'Andy', 'Monty', 'e7it0066bqvae2ce8qjfallggs', '3df730ce83611638eb077382ac4b002ed9ad87178db28325a68ac4d1e2d63662', 'no home', 'Verona', 'mantundrei@gmail.com', 'MNTNDR91Z03B296S' );
INSERT INTO Users VALUES ( 'marym', 'Mary', 'Monty', '4e8k0l6afln6b62c98g2a1c90u', 'bd779090185b596f5f2dbe87a375bca3161d7ce9a405a4b216de022029b56295', 'no home', 'Verona', 'mantundreisister@gmail.com', 'MNTMRA93E03B296S' );
INSERT INTO Users VALUES ( 'saraj', 'Sarah', 'Julian', '7mjioqd3pu9b0hh8l7co7a49bb', 'd1ac398f3327ec460ebb2714a112d9dc80898220ccde342b566a45b6836ee8b2', 'St. Lucy', 'Verona', 'saraj@gmail.com', null);
INSERT INTO Users VALUES ( 'taccarlo', 'Carl', 'Tackle', 's9hahkce12iesi06u37lhbats7', '24f6117cae8adfae4429b02f54577ed76d644ffbc3178b1c4b56eea55c416560', 'Grey Zone', 'Verona', 'taccarlo@hotmail.com', null );
INSERT INTO Users VALUES ( 'mennyrally', 'Menny', 'Rally', '8loa3qjdlg2rmf55p21270olvv', 'cff730d474a8dd9b0858115ff575a06356584a3049726d49d4330f43e8470f56', 'Peruvian Boulevard', 'Lima', 'mennyrally@hotmail.com', null );
INSERT INTO Users VALUES ( 'castle', 'Nick', 'Castle', 'kcisdnm1f2q8cja3hivlif39l4', '767a39ada9a5136306cd28be5494429163cf9518a9636b6df2fbf9fedb3617d2', 'Lugo of Grey Zone', 'Verona', 'nickcastle@hotmail.com', null );
INSERT INTO Users VALUES ( 'vincy', 'Vincent', 'Archer', 'odu0pkrsmog8s7k0s63sn7rvlo', 'b4c7f62b6ffb9c868618403319b72ad91f36be3ee71e3eee1faf673f8fd84ec9', 'Isis', 'Verona', 'archer@isis.com', null );
INSERT INTO Users VALUES ( 'peterbigface', 'Peter', 'Muson', 'ob3f1gou0hipcva3j76fl0bf9t', '8ea20ac664da9b613470ecfaf0bc89a77db8627bb8bf86663fb730754cc4c235', 'Grey Zone', 'Verona', 'muson@peter.com', null );

INSERT INTO Category VALUES ('vh','Vehicles');
INSERT INTO Category VALUES ('mi','Musical Instruments');
INSERT INTO Category VALUES ('mu','Music');
INSERT INTO Category VALUES ('cl','Clothes');
INSERT INTO Category VALUES ('cp','Cellphones');
INSERT INTO Category VALUES ('vp','Vehicle Parts');
INSERT INTO Category VALUES ('pp','PC Parts');
INSERT INTO Category VALUES ('vg','Videogames');
INSERT INTO Category VALUES ('ts','Toys');
INSERT INTO Category VALUES ('bk','Books');
INSERT INTO Category VALUES ('mv','Movies');
INSERT INTO Category VALUES ('nt','Antiques');

INSERT INTO Object VALUES ('eightnine', '0', 'Ibanez RG105 Prestige', 'Ibanez RG105 Prestige barely used', 'Ibanez_RG105_Prestige.jpg', 'Musical Instruments');
INSERT INTO Object VALUES ('eightnine', '1', 'Epiphone Gibson', 'Epiphone Gibson barely used', 'Epiphone_Gibson.jpg', 'Musical Instruments');
INSERT INTO Object VALUES ('eightnine', '2', 'Roland Keyboard', 'Roland Keyboard NEW', 'Roland_Keyboard.jpg', 'Musical Instruments');
INSERT INTO Object VALUES ('eightnine', '3', 'Asus P5 Motherboard', 'Asus P5 Motherboard used','AsusP5.jpg' , 'PC Parts');
INSERT INTO Object VALUES ('eightnine', '4', 'Intel i7 processor', 'Intel i7 processor used, busted and useless', null, 'PC Parts');
INSERT INTO Object VALUES ('eightnine', '5', 'Quake III', 'Quake III NEW', 'quake3.jpg', 'Videogames');
INSERT INTO Object VALUES ('eightnine','6','Magic The Gathering','Magic the Gathering play set','Magic_The_Gathering','Videogames');
INSERT INTO Object VALUES ('eightnine','7','Goedel, Escher, Bach','Best book ever','GAB.jpg','Books');
INSERT INTO Object VALUES ('eightnine','8','Barneys Version','second best book ever',null,'Books');
INSERT INTO Object VALUES ('eightnine','9','Java Essentials','for java beginners',null,'Books');
INSERT INTO Object VALUES ('eightnine','10','Compilers','too much stuff for me',null,'Books');
INSERT INTO Object VALUES ('eightnine','11','Nokia Lumia','Windows Phone','Nokia_Lumia.jpg','Cellphones');
INSERT INTO Object VALUES ('eightnine','12','Pirates Of The Carribeans','DVD',null,'Movies');
INSERT INTO Object VALUES ('eightnine','13','Renault Megane','Renault Megane barely used',null,'Vehicles');
INSERT INTO Vehicle VALUES ('eightnine','13','2005','ZR203XV','MRSNCL83C25B296S');
INSERT INTO Object VALUES ('eightnine','14','Testament - The Gathering','awesome band, awesome CD',null,'Music');

INSERT INTO Object VALUES ('qusdu', '0', 'Assassins Creed IV', 'Assassins Creed IV original copy NEW', null, 'Videogames');
INSERT INTO Object VALUES ('qusdu', '1', 'Tribes Ascend', 'Tribes Ascend online key codes included', null, 'Videogames');
INSERT INTO Object VALUES ('qusdu', '2', 'Tribes Ascend 2', 'Tribes Ascend 2 online key codes included', null, 'Videogames');
INSERT INTO Object VALUES ('qusdu','3','Fujitsu S3','Flat Monitor',null,'PC Parts');
INSERT INTO Object VALUES ('qusdu','4','Folding Chair','nice old folding chair, heavily used',null,'Antiques');
INSERT INTO Object VALUES ('qusdu','5','Unique Teapot','Old Teapot, it belonged to my aunt',null,'Antiques');
INSERT INTO Object VALUES ('qusdu','6','Nice Tarp','old tarp. No idea where it came from.',null,'Antiques');
INSERT INTO Object VALUES ('qusdu','7','Nokia 3210','it still works. Magic.',null,'Antiques');
INSERT INTO Object VALUES ('qusdu','8','Beautiful Lamp','Old lamp, it belonged to my aunt. Im not robbing my aunt, swear.',null,'Antiques');
INSERT INTO Object VALUES ('qusdu','9','Used Sheets','Barely used, I think. I might be robbing my aunt for these.',null,'Antiques');
INSERT INTO Object VALUES ('qusdu','10','Nice Painting','old painting I found somewhere. Not at my aunts.',null,'Antiques');
INSERT INTO Object VALUES ('qusdu','11','Stamp Collection','huge collection of antique stamps',null,'Antiques');

INSERT INTO Object VALUES ('marym','0','Party Dress','nice dress for parties',null,'Clothes');
INSERT INTO Object VALUES ('marym','1','Party Shoes','nice shoes for parties',null,'Clothes');
INSERT INTO Object VALUES ('marym','2','cargo Pants','not sure why I have these',null,'Clothes');
INSERT INTO Object VALUES ('marym','3','Samsung Galaxy Chat','it still works, kinda. Cant access SD anymore',null,'Cellphones');
INSERT INTO Object VALUES ('marym','4','Barbie Doll','selling my childhood feels right',null,'Toys');
INSERT INTO Object VALUES ('marym','5','Ken Doll','cant sell barbie without her partner',null,'Toys');
INSERT INTO Object VALUES ('marym','6','Digimon Dolls','Almost all Digimon dolls',null,'Toys');
INSERT INTO Object VALUES ('marym','7','The Usual Suspects DVD','DVD of one of the best movies ever',null,'Movies');
INSERT INTO Object VALUES ('marym','8','Independence Day DVD','DVD, still new',null,'Movies');
INSERT INTO Object VALUES ('marym','9','Catch 22','Good book, I have the kindle version now so I dont need it anymore',null,'Books');
INSERT INTO Object VALUES ('marym','10','Dune','probably the best book ever',null,'Books');
INSERT INTO Object VALUES ('marym','11','Goedel, Escher, Bach','GEB, EGB, a masterpiece',null,'Books');

INSERT INTO Auction VALUES ('eightnine','0','2014-06-12 13:04:01','2014-07-12 13:04:01','1500','140','saraj');
INSERT INTO Auction VALUES ('eightnine','1','2014-06-12 13:04:34','2014-08-10 11:18:34','890','140',null);
INSERT INTO Auction VALUES ('eightnine','2','2014-06-12 13:05:45','2014-08-12 13:05:45','240','30',null);
INSERT INTO Auction VALUES ('eightnine','3','2014-06-12 13:07:25','2014-08-12 13:07:25','140','10',null);
INSERT INTO Auction VALUES ('eightnine','4','2014-06-12 13:08:25','2014-08-12 13:08:25','240','10',null);
INSERT INTO Auction VALUES ('eightnine','5','2014-06-12 13:12:55','2014-08-12 13:12:55','30','10',null);
INSERT INTO Auction VALUES ('eightnine','6','2014-06-12 18:02:11','2014-08-12 18:02:11','12','0',null);
INSERT INTO Auction VALUES ('eightnine','7','2014-06-23 10:22:41','2014-08-23 10:22:41','10','0',null);
INSERT INTO Auction VALUES ('eightnine','8','2014-06-23 13:05:45','2014-08-23 13:05:45','10','0',null);
INSERT INTO Auction VALUES ('eightnine','9','2014-06-23 13:07:25','2014-08-23 13:07:25','10','0',null);
INSERT INTO Auction VALUES ('eightnine','10','2014-06-23 18:02:14','2014-08-23 18:02:14','5','0',null);
INSERT INTO Auction VALUES ('eightnine','11','2014-06-23 22:02:33','2014-08-23 22:02:33','120','10',null);
INSERT INTO Auction VALUES ('eightnine','12','2014-06-23 22:32:13','2014-08-23 22:32:13','14','0',null);
INSERT INTO Auction VALUES ('eightnine','13','2014-06-24 10:22:41','2014-08-24 10:22:41','9450','0',null);
INSERT INTO Auction VALUES ('eightnine','14','2014-06-24 13:07:25','2014-08-24 13:07:25','10','0',null);


INSERT INTO Auction VALUES ('qusdu','0','2014-06-10 13:05:45','2014-08-10 13:05:45','40','10',null);
INSERT INTO Auction VALUES ('qusdu','1','2014-06-10 13:07:25','2014-08-10 13:07:25','20','0',null);
INSERT INTO Auction VALUES ('qusdu','2','2014-06-10 13:12:55','2014-08-10 13:12:55','30','0',null);
INSERT INTO Auction VALUES ('qusdu','3','2014-06-11 18:02:11','2014-08-11 18:02:11','200','20',null);
INSERT INTO Auction VALUES ('qusdu','4','2014-06-11 10:22:41','2014-08-11 10:22:41','30','0',null);
INSERT INTO Auction VALUES ('qusdu','5','2014-06-11 10:24:41','2014-08-11 10:24:41','50','0',null);
INSERT INTO Auction VALUES ('qusdu','6','2014-06-11 22:02:33','2014-07-11 22:02:33','200','30',null);
INSERT INTO Auction VALUES ('qusdu','7','2014-06-11 22:04:23','2014-08-11 22:04:23','10','0',null);
INSERT INTO Auction VALUES ('qusdu','8','2014-06-11 22:23:44','2014-08-11 22:23:44','80','20',null);
INSERT INTO Auction VALUES ('qusdu','9','2014-06-11 22:43:24','2014-08-11 22:43:24','10','0',null);
INSERT INTO Auction VALUES ('qusdu','10','2014-06-11 13:05:45','2014-08-11 13:05:45','300','10',null);
INSERT INTO Auction VALUES ('qusdu','11','2014-06-11 13:07:25','2014-08-11 13:07:25','80','10',null);


INSERT INTO Auction VALUES ('marym','0','2014-06-23 10:22:41','2014-08-23 10:22:41','120','10',null);
INSERT INTO Auction VALUES ('marym','1','2014-06-23 10:25:41','2014-08-23 10:25:41','80','10',null);
INSERT INTO Auction VALUES ('marym','2','2014-06-23 10:29:41','2014-08-23 10:29:41','30','0',null);
INSERT INTO Auction VALUES ('marym','3','2014-04-23 13:05:45','2014-05-23 13:05:45','80','0','nowinner');
INSERT INTO Auction VALUES ('marym','3','2014-06-24 13:06:45','2014-08-24 13:06:45','70','0',null);
INSERT INTO Auction VALUES ('marym','4','2014-06-23 13:07:25','2014-08-23 13:07:25','10','0',null);
INSERT INTO Auction VALUES ('marym','5','2014-06-23 13:12:55','2014-08-23 13:12:55','10','0',null);
INSERT INTO Auction VALUES ('marym','6','2014-06-23 18:02:11','2014-08-23 18:02:11','20','0',null);
INSERT INTO Auction VALUES ('marym','7','2014-06-30 18:02:11','2014-08-30 18:02:11','20','0',null);
INSERT INTO Auction VALUES ('marym','8','2014-06-30 18:02:51','2014-08-30 18:02:51','10','0',null);
INSERT INTO Auction VALUES ('marym','9','2014-06-14 18:02:34','2014-08-14 18:02:34','10','0',null);
INSERT INTO Auction VALUES ('marym','10','2014-06-14 22:23:44','2014-08-14 22:23:44','10','0',null);
INSERT INTO Auction VALUES ('marym','11','2014-06-14 22:25:14','2014-08-16 11:18:14','10','0',null);

INSERT INTO Offer VALUES ('qusdu','eightnine','0','2014-06-12 13:04:01','2014-06-12 13:04:11','1550');
INSERT INTO Offer VALUES ('marym','eightnine','0','2014-06-12 13:04:01','2014-06-14 11:00:23','1680');
INSERT INTO Offer VALUES ('taccarlo','eightnine','0','2014-06-12 13:04:01','2014-06-14 12:02:03','1720');
INSERT INTO Offer VALUES ('vincy','eightnine','0','2014-06-12 13:04:01','2014-06-22 11:11:11','2000');
INSERT INTO Offer VALUES ('saraj','eightnine','0','2014-06-12 13:04:01','2014-06-30 23:02:01','2500');
INSERT INTO Offer VALUES ('mennyrally','eightnine','1','2014-06-12 13:04:34','2014-06-13 13:03:00','900');

INSERT INTO Offer VALUES ('vincy','eightnine','1','2014-06-12 13:04:34','2014-06-14 22:02:11','1000');
INSERT INTO Offer VALUES ('taccarlo','eightnine','1','2014-06-12 13:04:34','2014-06-14 23:00:11','1001');
INSERT INTO Offer VALUES ('castle','eightnine','1','2014-06-12 13:04:34','2014-06-16 10:03:24','1300');
INSERT INTO Offer VALUES ('marym','eightnine','1','2014-06-12 13:04:34','2014-06-24 13:04:12','1650');
INSERT INTO Offer VALUES ('mennyrally','eightnine','2','2014-06-12 13:05:45','2014-06-13 23:02:11','300');
INSERT INTO Offer VALUES ('qusdu','eightnine','2','2014-06-12 13:05:45','2014-06-14 11:11:10','350');
INSERT INTO Offer VALUES ('peterbigface','eightnine','2','2014-06-12 13:05:45','2014-06-14 14:03:12','500');
INSERT INTO Offer VALUES ('eightnine','qusdu','0','2014-06-10 13:05:45','2014-06-11 15:02:11','50');
INSERT INTO Offer VALUES ('marym','qusdu','0','2014-06-10 13:05:45','2014-06-11 16:03:12','60');
INSERT INTO Offer VALUES ('taccarlo','qusdu','0','2014-06-10 13:05:45','2014-06-12 09:11:34','80');
INSERT INTO Offer VALUES ('eightnine','qusdu','1','2014-06-10 13:07:25','2014-06-11 13:04:02','40');
INSERT INTO Offer VALUES ('eightnine','qusdu','6','2014-06-11 22:02:33','2014-07-11 13:04:02','220');

INSERT INTO Payment VALUES('eightnine', '0', '2014-06-12 13:04:01', '2014-07-13 17:12:00', '2500', 'credit card');

