package test;

import java.io.FileWriter;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.math.BigInteger;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;
import java.util.Scanner;
import java.util.Vector;


public class SQLGenerator {
	private Scanner reader;
	private FileWriter out;
	String name;
	int nAttr = 0;
	String[] attrName;
	String[] attributes;
	boolean running = true;
	boolean more = true;
	Vector<String> sql;

	public SQLGenerator(){
		reader = new Scanner(System.in);
		try {
			out = new FileWriter("generated.sql",true);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		init();
	}

	private void init() {
		SecureRandom random = new SecureRandom();
		while(running){
			System.out.print("Name => ");
			name = reader.nextLine();
			System.out.print("how many attributes => ");
			nAttr = Integer.parseInt(reader.nextLine());
			attrName = new String[nAttr];
			attributes = new String[nAttr];
			for(int i = 0; i < nAttr;i++){
				System.out.print("Attribute n. " + (i + 1) + " => ");
				attrName[i] = reader.nextLine();
			}

			while(more){
				for(int i = 0; i < nAttr;i++){
					if(attrName[i].equals("salt")){
						System.out.print("Insert password to produce salt and hash: ");
						String passwd = reader.nextLine();
						String salt = new BigInteger(130, random).toString(32);
						String hash = generateHash(passwd,salt);
						attributes[i] = salt;
						attributes[i+1] = hash;
						System.out.println(attrName[i] + " => " +attributes[i]);
						System.out.println(attrName[i+1] + " => " + attributes[i+1]);
					}else if(attrName[i].equals("hash"))
						continue;
					else{
						System.out.print(attrName[i] + " => ");
						attributes[i] = reader.nextLine();
					}
				}
				generateSQL();
				System.out.print("More of this object? ");
				if(reader.nextLine().equals("no"))
					more = false;
			}
			System.out.print("New object? ");
			if(reader.nextLine().equals("no")){
				running = false;
				try {
					out.close();
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
			more = true;
		}
	}

	private void generateSQL() {
		String output = "INSERT INTO " + name + " VALUES (";
		for(int i = 0;i < (attributes.length -1) ;i++){
			if(attributes[i].equals("null"))
				output += attributes[i] + ",";
			else
				output += "'" + attributes[i] + "',";
		}

		if(attributes[attributes.length -1].equals("null"))
			output += attributes[attributes.length -1] + ");\n";
		else
			output += "'" + attributes[attributes.length -1] + "');\n";

		System.out.println(output);
		try {
			out.write(output);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		//		out.println(output);
	}
	private String generateHash(String password, String salt){
		String text = password+salt;
		MessageDigest md = null;
		try {
			md = MessageDigest.getInstance("SHA-256");
			md.update(text.getBytes("UTF-8"));
		} catch (NoSuchAlgorithmException e) {
			e.printStackTrace();
		} catch (UnsupportedEncodingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		byte[] digest = md.digest();

		StringBuilder sb = new StringBuilder(); 
		for(byte b : digest){ 
			sb.append(String.format("%02x", b&0xff)); 
		}
		return sb.toString();
	}

	public static void main(String[] args){
		new SQLGenerator();		
	}

}
