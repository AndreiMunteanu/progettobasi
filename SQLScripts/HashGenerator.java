import java.io.UnsupportedEncodingException;
import java.math.BigInteger;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;

public class HashGenerator {

	public static void main(String[] args) throws UnsupportedEncodingException {
		String password = "password";
		for(int i = 0; i < 9; i++)
			printHashAndSalt(password);

	}

	private static void printHashAndSalt(String password){
		SecureRandom random = new SecureRandom();

		String salt = new BigInteger(130, random).toString(32);
		System.out.println("pass: "+password);
		System.out.print("salt: '" +salt+"'");
		MessageDigest md = null;
		String text = password+salt;
		try {
			md = MessageDigest.getInstance("SHA-256");
			md.update(text.getBytes("UTF-8"));
		} catch (NoSuchAlgorithmException e) {
			e.printStackTrace();
		} catch (UnsupportedEncodingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		byte[] digest = md.digest();

		StringBuilder sb = new StringBuilder(); 
		for(byte b : digest){ 
			sb.append(String.format("%02x", b&0xff)); 
		}
		System.out.println(", '"+sb.toString()+"'");

	}

	private void validLogin(String username,String password){
		SecureRandom random = new SecureRandom();

		String salt = new BigInteger(130, random).toString(32);
		System.out.print("salt: '" +salt+"'");
		MessageDigest md = null;
		String text = password+salt;
		try {
			md = MessageDigest.getInstance("SHA-256");


			md.update(text.getBytes("UTF-8"));
		} catch (NoSuchAlgorithmException e) {
			e.printStackTrace();
		} catch (UnsupportedEncodingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		byte[] digest = md.digest();

		StringBuilder sb = new StringBuilder(); 
		for(byte b : digest){ 
			sb.append(String.format("%02x", b&0xff)); 
		}
		System.out.println(", '"+sb.toString()+"'");
	}

}
