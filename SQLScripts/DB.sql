DROP TABLE Offer;
DROP TABLE Payment;
DROP TABLE Auction;
DROP TABLE Vehicle;
DROP TABLE Object;
DROP TABLE Category;
DROP TABLE Users;
DROP TYPE payment_type;

CREATE TYPE payment_type AS ENUM (
	'credit card', 'check', 'bank transaction'
);

CREATE TABLE Users (
	username varchar(20) PRIMARY KEY,
	name varchar(30) NOT NULL,
	surname varchar(30) NOT NULL,
	salt varchar(28) NOT NULL,
	hash varchar(64) NOT NULL,
	address varchar(50) NOT NULL,
	city varchar(30) NOT NULL,
	email varchar(100) NOT NULL CHECK(email LIKE '%_@_%.__%'),	
	CF varchar(16)
	);

CREATE TABLE Category (
	id varchar(2),
	name varchar(30),
	PRIMARY KEY(name)
	);
	
CREATE TABLE Object (
	username varchar(20) REFERENCES Users(username)
	ON UPDATE CASCADE
	ON DELETE SET NULL,
	code integer,
	name varchar(40) NOT NULL,
	description varchar(100) NOT NULL,
	photo varchar(50),
	category varchar(40) REFERENCES Category(name)
	ON UPDATE CASCADE
	ON DELETE SET NULL,
	PRIMARY KEY(username, code)
	);

CREATE TABLE Vehicle (
	username varchar(20),
	code integer,
	imm_year integer NOT NULL,
	plate_n varchar(10) NOT NULL,
	CF varchar(16) NOT NULL,
	FOREIGN KEY(username, code) REFERENCES Object(username, code)
	ON UPDATE CASCADE
	ON DELETE SET NULL,
	PRIMARY KEY(username, code)
	);
	
	
CREATE TABLE Auction (
	seller varchar(20),
	code integer,
	begin timestamp NOT NULL,
	end_time timestamp NOT NULL,
	base_price integer NOT NULL CHECK(base_price > 0),
	spedition_cost integer NOT NULL CHECK(spedition_cost >= 0),
	winner varchar(30) REFERENCES Users(username)
	ON UPDATE CASCADE
	ON DELETE SET NULL,
	CHECK(begin < end_time),
	PRIMARY KEY(seller, code, begin),
	FOREIGN KEY(seller, code) REFERENCES Object(username, code)
	ON UPDATE CASCADE
	ON DELETE SET NULL
	 );

CREATE TABLE Payment (
	seller varchar(20),
	code integer,
	begin timestamp,
	date timestamp NOT NULL,
	sum integer NOT NULL CHECK(sum > 0),
	type payment_type,
	FOREIGN KEY(seller, code, begin) REFERENCES Auction(seller, code, begin)
	ON UPDATE CASCADE
	ON DELETE SET NULL
	);
	
CREATE TABLE Offer (
	buyer varchar(20) REFERENCES Users(username)
	ON UPDATE CASCADE
	ON DELETE SET NULL,
	seller varchar(20),
	code integer,
	begin timestamp,
	time timestamp NOT NULL CHECK(begin < time),
	sum integer NOT NULL CHECK(sum > 0),
	FOREIGN KEY(seller, code, begin) REFERENCES Auction(seller, code, begin)
	ON UPDATE CASCADE
	ON DELETE SET NULL,
	PRIMARY KEY(buyer, seller, code, begin, time)
	);
