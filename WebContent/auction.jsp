<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
    <%@ page import="java.sql.*" %>
    <%@ page import="java.util.Date" %>
    <%@ page import ="java.text.SimpleDateFormat" %>
<%@ page import="java.util.*" %>
<%@ page import="did.*" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<%
	Vector<OfferBean> offers = (Vector<OfferBean>)request.getAttribute("offers");
	AuctionBean auction = (AuctionBean)request.getAttribute("auction");
	Timestamp now = new Timestamp(new Date().getTime());
	Timestamp endTime = Timestamp.valueOf(auction.getEnd_time());
	SimpleDateFormat dateFormat = new SimpleDateFormat("dd-MM-YYYY HH:mm:ss");
%>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<link rel="stylesheet" href="../styles.css" type="text/css" media="screen">
<title>Auction for <%=auction.getObject() %></title>
</head>
<body>
<div id="main">
<div id="header">
			<a href="/ProgettoBasi/servlet/Main"><img src="../images/logo.png"></a>
</div>
<center> <h2> <%=auction.getObject() %></h2></center>
Description: <%=auction.getDesc() %><br/>
Base Price: <%=auction.getBase_price() %><br/>
<% if(auction.getPhoto() != null){ %>
Photo: <img src="../images/<%=auction.getSeller()%>/<%=auction.getPhoto()%>"/><br/>
<%} %>
<center>Offers:<br/></center>
<ol>
<%
	if(offers != null)
	for(OfferBean o : offers){
%>
	<li><%=o.getSum() %>€ at <%=dateFormat.format(Timestamp.valueOf(o.getTime())) %> by <%=o.getBuyer() %> </li>
<%
	}
	else{
%>
No results
<%
	}
%>
</ol>
<%if(endTime.after(now)){ %>
<form action="Main" method="POST">
<input type="hidden" name="ps" value="off" />
<label>Username: </label><input type="text" name="username">
<br />
<label>Password: </label> <input type="password" name="password" />
<input type="submit" value="Submit" />
<input type="hidden" name="seller" value="<%=request.getParameter("seller")%>" />
<input type="hidden" name="code" value="<%=request.getParameter("code")%>" />
<input type="hidden" name="begin" value="<%=request.getParameter("begin") %>" />  
<input type="hidden" name="objName" value="<%=auction.getObject() %>" />  
<input type="hidden" name="objDesc" value="<%=auction.getDesc() %>" />  
</form>
<%}
else {
%>
<center><h2>This auction has ended!  
<%if(offers != null && offers.size() != 0){%>
User <%=offers.lastElement().getBuyer() %> has won.
<% }
%></h2></center> 
<%} %>
<div id="footer"> 
			<a href="/ProgettoBasi/servlet/Main">Home</a> <b>||</b>
			© <b>Auction Caution</b> (via le Grazie)  <b>||</b>  <b>Contacts:</b> <a href="mailto:niccolo.marastoni@studenti.univr.it"><b>Niccolò Marastoni</a> || <a href="mailto:andrei.munteanu@studenti.univr.it">Andrei Munteanu</b></a>
		</div> 
</div>
</body>
</html>