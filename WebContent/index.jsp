<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ page import="java.sql.*" %>
<%@ page import="java.util.*" %>
<%@ page import="did.*" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<%
	Vector<CategoryBean> categories = (Vector<CategoryBean>)request.getAttribute("categories");
%>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<link rel="stylesheet" href="../styles.css" type="text/css" media="screen">
<title>Auction Caution - HomePage</title>
</head>
<body>

<div id="main">
<div id="header">
			<a href="#"><img src="../images/logo.png"></a>
</div>
	
<div class="info">
Welcome to AuctionCaution.com, the best site on the web if you want to get rid of your stuff! <br/>
<b>Rules:</b><br/>
<ol>
<li>
If you want to buy other people's stuff, join in and make offers!<br/></li>
<li>
If you want to sell your stuff, we're gonna need your Fiscal Code.</li>
<li>That's it.</li>
</ol>
</div>
	
<div class="desc">
<h1> Categories:</h1>
<u>
<%
	for(CategoryBean c : categories){
%>
	<li> 
	<!-- not a link if ther aren't any items on sale for this category -->
	<%if(c.getCount() > 0){ %>
	<a href="/ProgettoBasi/servlet/Main?ps=cat&id=<%=c.getId() %>"><%=c.getName()%></a> (<%=c.getCount() %>)
	<%}
	else
	{ %>
	<%=c.getName()%> (<%=c.getCount() %>)
	
	<%} %>
	</li>
<%
	}
%>
</u>
</div>
<div id="footer"> 
			<a href="#">Home</a> <b>||</b>
			© <b>Auction Caution</b> (via le Grazie)  <b>||</b>  <b>Contacts:</b> <a href="mailto:niccolo.marastoni@studenti.univr.it"><b>Niccolò Marastoni</a> || <a href="mailto:andrei.munteanu@studenti.univr.it">Andrei Munteanu</b></a>
		</div> 
</div>

</body>
</html>
