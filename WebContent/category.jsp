<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ page import="java.sql.*" %>
<%@ page import="java.util.*" %>
<%@ page import ="java.text.SimpleDateFormat" %>
<%@ page import="did.*" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<%
	String title = (String)request.getAttribute("title");
	Vector<AuctionBean> auctions = (Vector<AuctionBean>)request.getAttribute("auctions");
	SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
%>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<link rel="stylesheet" href="../styles.css" type="text/css" media="screen">
<title><%=title %></title>
</head>
<body>
<div id="main">
<div id="header">
			<a href="/ProgettoBasi/servlet/Main"><img src="../images/logo.png"></a>
</div>
	<center><h1><%=title %></h1></center>
<div class="info">
<ol>
<%
	for(AuctionBean a : auctions){
		if(a.getWinner() == null){
%>
	<li><a href="/ProgettoBasi/servlet/Main?ps=auc&seller=<%=a.getSeller()%>&code=<%=a.getCode()%>&begin=<%=a.getBegin()%>"><%=a.getObject() %></a>- (<%=a.getBase_price() %>€) sold by <%=a.getSeller() %>. <%=a.getOffers_number()%> offers. last offer: <%=a.getLast_offer() %> <br/> <%=dateFormat.format(Timestamp.valueOf(a.getBegin())) %> <%=dateFormat.format(Timestamp.valueOf(a.getEnd_time())) %><br/><%=a.getDesc() %></li>
<%
		}
	}
%>
</ol>
</div>
<div id="footer"> 
			<a href="/ProgettoBasi/servlet/Main">Home</a> <b>||</b>
			© <b>Auction Caution</b> (via le Grazie)  <b>||</b>  <b>Contacts:</b> <a href="mailto:niccolo.marastoni@studenti.univr.it"><b>Niccolò Marastoni</a> || <a href="mailto:andrei.munteanu@studenti.univr.it">Andrei Munteanu</b></a>
		</div> 
</div>
</body>
</html>