<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ page import="java.sql.Timestamp" %>
<%@ page import="java.util.Date" %>
<%@ page import ="java.text.SimpleDateFormat" %>
<%@ page import="did.*" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<%
	AuctionBean auction = (AuctionBean) request.getAttribute("auction");
	SimpleDateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss");
	Timestamp endTime = Timestamp.valueOf((auction.getEnd_time()));
	long remainingTime =  endTime.getTime() - (new Date().getTime());
	
	long remainingTimeAux = remainingTime / 1000;
	long seconds = remainingTimeAux % 60;
	remainingTimeAux /= 60;
	long minutes = remainingTimeAux % 60;
	remainingTimeAux /= 60;
	long hours = remainingTimeAux % 24;
	remainingTimeAux /= 24;
	long days = remainingTimeAux;
	
	String endString = dateFormat.format(endTime);
	String[] dayAndTime = endString.split(" ");
%>
<head>
<script type="text/javascript"> 
		var message = "";
		function checkOffer(){ 
			var regExp = new RegExp("[0-9]+");
			var base_price = "<%=auction.getBase_price()%>";
			var last_offer = "<%=auction.getLast_offer()%>";
			var offer = document.forms["offerForm"]["offer"].value;
			if(!regExp.test(offer)){
				document.getElementById("error-message").innerHTML = "Insert a digit!!!";
				return false;
			}
			else if (last_offer == 0 && parseInt(offer) <= base_price) {
				document.getElementById("error-message").innerHTML = "Your offer of " + offer + "€ is lower than the base price: " + base_price + "€";
				return false;
			} 
			else if (parseInt(offer) <= last_offer) {
				document.getElementById("error-message").innerHTML = "Your offer of " + offer + "€ is lower than the last offer: " + last_offer + "€";
				return false;
			}
			return true;
		}
</script>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<link rel="stylesheet" href="../styles.css" type="text/css" media="screen">
<title>Offer</title>
</head>
<body>
<div id="main">
<div id="header">
			<a href="/ProgettoBasi/servlet/Main"><img src="../images/logo.png"></a>
</div>
	<center>
		<h2><%=request.getParameter("objName")%></h2>
	</center>
	<br /> Description:
	<%=request.getParameter("objDesc")%>.<br/>
	Base price: <%=auction.getBase_price() %><br/>
		<%if(auction.getLast_offer() > 0){
		%>
		Last offer: <%=auction.getLast_offer() %><br/>
		
		<%} %>
	<%if(remainingTime > 100) {%>
	This offer will end on the <%=dayAndTime[0] %> at <%=dayAndTime[1] %>, in 
	<div id="countdown-timer"><div id="days">	</div> days, <div id="hours"></div>:<div id="minutes"></div>:<div id="seconds"></div></div>
	<form name="offerForm" action="Main" method="POST" 
		onSubmit="return checkOffer()">
		<br /> Make an offer: <input type="text" name="offer" /> 
			<input type="hidden" name="ps" value="voff" />
			<input type="hidden" name="seller" value="<%=request.getParameter("seller")%>" /> 
			<input type="hidden" name="code" value="<%=request.getParameter("code")%>" />
			<input type="hidden" name="begin"value="<%=request.getParameter("begin")%>" /> 
			<input type="hidden" name="username"value="<%=request.getParameter("username")%>" /> 
			<input type="submit" value="Submit" /> <div id="error-message"> </div>
	</form>
<%} else{ %>
<br/><center><h2>This auction has ended!</h2></center><br/>
<%} %>
<div id="footer"> 
			<a href="/ProgettoBasi/servlet/Main">Home</a> <b>||</b>
			© <b>Auction Caution</b> (via le Grazie)  <b>||</b>  <b>Contacts:</b> <a href="mailto:niccolo.marastoni@studenti.univr.it"><b>Niccolò Marastoni</a> || <a href="mailto:andrei.munteanu@studenti.univr.it">Andrei Munteanu</b></a>
		</div> 
		</div>
<script> 
<!-- countdown-timer
// 
 var seconds=<%=seconds%>
 var minutes=<%=minutes%>
 var hours=<%=hours%>
 var days=<%=days%>
 document.getElementById("seconds").innerHTML='30' 

function display(){ 
 if (seconds<=-1){ 
    seconds=59
    minutes-=1 
 } 
 if (minutes<=-1){ 
    minutes=59
    hours-=1 
 } 
 if (hours<=-1){
	days-=1
	hours=23
}
 if(days==0 && hours==0 && minutes==0 && seconds==0){
	 document.getElementById("seconds").innerHTML=seconds
	 document.getElementsByName("offerForm")[0].innerHTML=""
	 document.getElementById("countdown-timer").innerHTML="<br/><center><h2>This auction has ended!</h2></center><br/>"
 }
 else 
    document.getElementById("days").innerHTML=days
    document.getElementById("hours").innerHTML=hours
    document.getElementById("minutes").innerHTML=minutes
    document.getElementById("seconds").innerHTML=seconds
    seconds-=1
    setTimeout("display()",1000) 
} 
display() 
--> 
</script> 
</body>
</html>