package did;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.Date;
import java.util.Vector;


public class DBMS {
		private String user = "nick";
		private String passwd = "password";
//	private String user = "qusdu";
//	private String passwd = "andreamunteanu";
	private String url = "jdbc:postgresql://localhost/dblab05";
	private String driver = "org.postgresql.Driver";

	private String cat = "SELECT C.id as id, C.name as cat_name, count(A.seller) as auction_n FROM Category C LEFT JOIN Object O ON(O.category = C.name) LEFT JOIN Auction A ON (O.username = A.seller AND O.code = A.code AND A.end_time > ? ) GROUP BY C.name ORDER BY C.name";
	//auctions by category, with added control for old auctions (it shouldn't return those who have winners) 
	private String abc = "SELECT O.name AS object, O.description as desc, O.photo as photo, A.code as code, A.seller AS seller, A.begin AS begin, A.end_time as end_time, A.base_price AS base_price, A.winner as winner, COUNT(off.code) AS offers_number,MAX(sum) AS last_offer FROM Category C JOIN Object O ON(C.name = O.category) JOIN Auction A ON(O.username = A.seller AND O.code = A.code) LEFT JOIN Offer off ON (a.seller = off.seller AND a.code = off.code AND a.begin = off.begin ) WHERE C.id = ? AND A.winner ISNULL GROUP BY O.name, O.description, O.photo, A.code, A.seller, A.begin, A.base_price";
	//get auction by seller, code, begin
	private String auc = "SELECT O.name AS object, O.description as desc, O.photo as photo, A.code as code, A.seller AS seller, A.begin AS begin, A.end_time as end_time, A.base_price AS base_price, A.winner as winner, COUNT(off.code) AS offers_number,MAX(sum) AS last_offer  FROM Object O JOIN Auction A ON(O.username = A.seller AND O.code = A.code) LEFT JOIN Offer off ON (a.seller = off.seller AND a.code = off.code AND a.begin = off.begin ) WHERE A.seller = ? AND A.code = ? AND A.begin = ? GROUP BY O.name, O.description, O.photo, A.code, A.seller, A.begin, A.base_price";
	//offers by auction
	private String oba = "SELECT buyer, sum, time FROM Offer WHERE seller = ? AND code = ? AND begin = ? ORDER BY time";

	private String ct = "SELECT name FROM Category WHERE id = ?";

	//retrieves the salt of the given username
	private String salt = "SELECT salt FROM Users WHERE username = ?";

	//retrieves the hash of the given username
	private String hash = "SELECT hash FROM Users WHERE username = ?";

	//retrieves the last offer and the base price of the give auction
	private String loabp = "SELECT max(sum) last_offer, base_price, end_time FROM Auction a LEFT JOIN Offer o ON(o.seller = a.seller AND o.code = a.code AND o.begin = a.begin) WHERE a.seller = ? AND a.code = ? AND a.begin = ? GROUP BY a.seller, base_price, end_time";
	//inserts an offer
	private String inso = "INSERT INTO Offer VALUES (?, ?, ?, ?, ?, ?)";
	//Set Auction Winner
	private String saw = "UPDATE Auction SET winner=? WHERE seller = ? AND code = ? AND begin = ?";

	public DBMS() throws ClassNotFoundException {
		Class.forName(driver);
	}

	public Vector<CategoryBean> getCategories(){
		Connection con = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		Vector result = new Vector();
		try {
			con = DriverManager.getConnection(url, user, passwd);
			pstmt = con.prepareStatement(cat); 
			pstmt.clearParameters();
			pstmt.setTimestamp(1, new Timestamp(new Date().getTime()));
			rs=pstmt.executeQuery(); 
			while(rs.next())
				result.add(makeCategoryBean(rs));
		} catch(SQLException sqle) {               
			sqle.printStackTrace();
		} finally {                                 
			try {
				con.close();
			} catch(SQLException sqle1) {
				sqle1.printStackTrace();
			}
		}
		return result;		

	}

	private CategoryBean makeCategoryBean(ResultSet rs) throws SQLException {
		CategoryBean bean = new CategoryBean();
		bean.setId(rs.getString("id"));
		bean.setCount(rs.getInt("auction_n"));
		bean.setName(rs.getString("cat_name"));
		return bean;
	}

	public Vector<AuctionBean> getAuctionsByCat(String id) {
		Connection con = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		Vector result = new Vector();	
		try {
			con = DriverManager.getConnection(url, user, passwd);
			pstmt = con.prepareStatement(abc); 
			pstmt.clearParameters();
			pstmt.setString(1, id); 
			rs=pstmt.executeQuery(); 
			while(rs.next())
				result.add(makeAuctionBean(rs));
		} catch(SQLException sqle) {                
			sqle.printStackTrace();
		} finally {                                 
			try {
				con.close();
			} catch(SQLException sqle1) {
				sqle1.printStackTrace();
			}
		}
		return result;
	}

	private AuctionBean makeAuctionBean(ResultSet rs) throws SQLException {
		AuctionBean bean = new AuctionBean();
		bean.setBase_price(rs.getInt("base_price"));
		bean.setDesc(rs.getString("desc"));
		bean.setObject(rs.getString("object"));
		bean.setBegin(rs.getString("begin"));
		bean.setEnd_time(rs.getString("end_time"));
		bean.setCode(rs.getInt("code"));
		bean.setSeller(rs.getString("seller"));
		bean.setOffers_number(rs.getInt("offers_number"));
		bean.setLast_offer(rs.getInt("last_offer"));
		bean.setWinner(rs.getString("winner"));
		bean.setPhoto(rs.getString("photo"));
		//if the auction has ended already and the winner hasn't been set
		//set the winner in the db
		if(Timestamp.valueOf(bean.getEnd_time()).before(new Date())){
			String winner;
			Vector<OfferBean> offers = getOffersByAuction(bean.getSeller(), bean.getCode(), bean.getBegin());
			if(offers.size() != 0){
				winner = getOffersByAuction(bean.getSeller(), bean.getCode(), bean.getBegin()).lastElement().getBuyer();
				setAuctionWinner(bean, winner);
				bean.setWinner(winner);
			}
			else{
				winner = "nowinner";
				setAuctionWinner(bean, winner);
				bean.setWinner(winner);
			}
		}
		return bean;
	}

	private void setAuctionWinner(AuctionBean bean, String winner) {
		Connection con = null;
		PreparedStatement pstmt = null;
		try {
			con = DriverManager.getConnection(url, user, passwd);
			pstmt = con.prepareStatement(saw); 
			pstmt.clearParameters();
			pstmt.setString(2, bean.getSeller());
			pstmt.setInt(3, bean.getCode());
			pstmt.setTimestamp(4, Timestamp.valueOf(bean.getBegin()));
			pstmt.setString(1, winner);
			pstmt.executeUpdate(); 
		} catch(SQLException sqle) {                
			sqle.printStackTrace();
		} finally {                                 
			try {
				con.close();
			} catch(SQLException sqle1) {
				sqle1.printStackTrace();
			}
		}
	}

	public String getCategory(String id) {
		Connection con = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		String result = "";	
		try {
			con = DriverManager.getConnection(url, user, passwd);
			pstmt = con.prepareStatement(ct); 
			pstmt.clearParameters();
			pstmt.setString(1, id); 
			rs=pstmt.executeQuery(); 
			while(rs.next())
				result = rs.getString("name");
		} catch(SQLException sqle) {                
			sqle.printStackTrace();
		} finally {                                 
			try {
				con.close();
			} catch(SQLException sqle1) {
				sqle1.printStackTrace();
			}
		}
		return result;
	}

	public Vector<OfferBean> getOffersByAuction(String seller, int code, String begin) {
		Connection con = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		Vector<OfferBean> result = new Vector<OfferBean>();	
		try {
			con = DriverManager.getConnection(url, user, passwd);
			pstmt = con.prepareStatement(oba); 
			pstmt.clearParameters();
			pstmt.setString(1, seller);
			pstmt.setInt(2, code);
			pstmt.setTimestamp(3, Timestamp.valueOf(begin));
			rs=pstmt.executeQuery(); 
			while(rs.next())
				result.add(makeOfferBean(rs));
		} catch(SQLException sqle) {                
			sqle.printStackTrace();
		} finally {                                 
			try {
				con.close();
			} catch(SQLException sqle1) {
				sqle1.printStackTrace();
			}
		}
		return result;
	}

	private OfferBean makeOfferBean(ResultSet rs) throws SQLException {
		OfferBean bean = new OfferBean();
		bean.setBuyer(rs.getString("buyer"));
		bean.setSum(rs.getInt("sum"));
		bean.setTime(rs.getString("time"));
		return bean;
	}

	public String getSaltByUsername(String username) {
		Connection con = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		String result = null;
		try {
			con = DriverManager.getConnection(url, user, passwd);
			pstmt = con.prepareStatement(salt); 
			pstmt.clearParameters();
			pstmt.setString(1, username);
			rs=pstmt.executeQuery(); 
			while(rs.next())
				result = makeSalt(rs);
		} catch(SQLException sqle) {                
			sqle.printStackTrace();
		} finally {                                 
			try {
				con.close();
			} catch(SQLException sqle1) {
				sqle1.printStackTrace();
			}
		}
		return result;
	}

	private String makeSalt(ResultSet rs) throws SQLException {
		return (String)rs.getString("salt");
	}

	public Object getHashByUsername(String username) {
		Connection con = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		String result = null;
		try {
			con = DriverManager.getConnection(url, user, passwd);
			pstmt = con.prepareStatement(hash); 
			pstmt.clearParameters();
			pstmt.setString(1, username);
			rs=pstmt.executeQuery(); 
			while(rs.next())
				result = makeHash(rs);
		} catch(SQLException sqle) {                
			sqle.printStackTrace();
		} finally {                                 
			try {
				con.close();
			} catch(SQLException sqle1) {
				sqle1.printStackTrace();
			}
		}
		return result;
	}

	private String makeHash(ResultSet rs) throws SQLException {
		return (String)rs.getString("hash");
	}

	public AuctionBean getLastOfferAndBasePriceOfAuction(String seller, int code, String begin) {
		Connection con = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		AuctionBean result = null;
		try {
			con = DriverManager.getConnection(url, user, passwd);
			pstmt = con.prepareStatement(loabp); 
			pstmt.clearParameters();
			pstmt.setString(1, seller);
			pstmt.setInt(2, code);
			pstmt.setTimestamp(3, Timestamp.valueOf(begin));
			rs=pstmt.executeQuery(); 
			while(rs.next())
				result = makeLastOfferAndBasePrice(rs);
		} catch(SQLException sqle) {                
			sqle.printStackTrace();
		} finally {                                 
			try {
				con.close();
			} catch(SQLException sqle1) {
				sqle1.printStackTrace();
			}
		}
		return result;
	}

	private AuctionBean makeLastOfferAndBasePrice(ResultSet rs) throws SQLException {
		AuctionBean bean = new AuctionBean();
		bean.setLast_offer(rs.getInt("last_offer"));
		bean.setBase_price(rs.getInt("base_price"));
		bean.setEnd_time(rs.getString("end_time"));
		return bean;
	}

	public void insertNewOffer(String username, String seller, int code, String begin, int offer) {
		Connection con = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		try {
			con = DriverManager.getConnection(url, user, passwd);
			pstmt = con.prepareStatement(inso); 
			pstmt.clearParameters();
			pstmt.setString(1, username);
			pstmt.setString(2, seller);
			pstmt.setInt(3, code);
			pstmt.setTimestamp(4, Timestamp.valueOf(begin));
			pstmt.setTimestamp(5, new Timestamp(new Date().getTime()));
			pstmt.setInt(6, offer);
			pstmt.executeUpdate(); 
		} catch(SQLException sqle) {                
			sqle.printStackTrace();
		} finally {                                 
			try {
				con.close();
			} catch(SQLException sqle1) {
				sqle1.printStackTrace();
			}
		}
	}

	public AuctionBean getAuction(String seller, int code, String begin) {
		Connection con = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		AuctionBean result = null;
		try {
			con = DriverManager.getConnection(url, user, passwd);
			pstmt = con.prepareStatement(auc); 
			pstmt.clearParameters();
			pstmt.setString(1, seller);
			pstmt.setInt(2, code);
			pstmt.setTimestamp(3, Timestamp.valueOf(begin));
			rs=pstmt.executeQuery(); 
			rs.next();
			result = makeAuctionBean(rs);
		} catch(SQLException sqle) {                
			sqle.printStackTrace();
		} finally {                                 
			try {
				con.close();
			} catch(SQLException sqle1) {
				sqle1.printStackTrace();
			}
		}
		return result;
	}
}
