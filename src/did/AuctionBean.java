package did;

public class AuctionBean {
	private String seller = "";
	private int code = 0;
	private String begin = "";
	private String end_time = "";
	private String object = "";
	private String desc = "";
	private int base_price = 0;
	private int offers_number = 0;
	private int last_offer = 0; 
	private String winner = "";
	private String photo = "";
	public String getSeller() {
		return seller;
	}
	public void setSeller(String seller) {
		this.seller = seller;
	}
	public String getObject() {
		return object;
	}
	public void setObject(String object) {
		this.object = object;
	}
	public String getDesc() {
		return desc;
	}
	public void setDesc(String desc) {
		this.desc = desc;
	}
	public int getBase_price() {
		return base_price;
	}
	public void setBase_price(int base_price) {
		this.base_price = base_price;
	}
	public int getCode() {
		return code;
	}
	public void setCode(int code) {
		this.code = code;
	}
	public String getBegin() {
		return begin;
	}
	public void setBegin(String begin) {
		this.begin = begin;
	}
	public int getOffers_number() {
		return offers_number;
	}
	public void setOffers_number(int offers_number) {
		this.offers_number = offers_number;
	}
	public int getLast_offer() {
		return last_offer;
	}
	public void setLast_offer(int last_offer) {
		this.last_offer = last_offer;
	}
	public String getEnd_time() {
		return end_time;
	}
	public void setEnd_time(String end_time) {
		this.end_time = end_time;
	}
	public String getWinner() {
		return winner;
	}
	public void setWinner(String winner) {
		this.winner = winner;
	}
	public String getPhoto() {
		return photo;
	}
	public void setPhoto(String photo) {
		this.photo = photo;
	}
	
	
}
