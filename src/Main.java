import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.sql.Timestamp;
import java.util.Vector;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import did.AuctionBean;
import did.CategoryBean;
import did.DBMS;
import did.OfferBean;


public class Main extends HttpServlet{

	public void doGet(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {

		String ps = "";
		RequestDispatcher rd = null;
		if (request.getParameter("ps") != null) {
			ps = request.getParameter("ps");
		}

		try {
			DBMS dbms = new DBMS();
			//index
			if(ps.equals("")){
				Vector<CategoryBean> categories = dbms.getCategories();
				request.setAttribute("categories",categories);
				rd = request.getRequestDispatcher("../index.jsp");
			}
			//Categories
			else if(ps.equals("cat")){
				String id = request.getParameter("id");
				String title = dbms.getCategory(id);
				Vector<AuctionBean> auctions = dbms.getAuctionsByCat(id);
				request.setAttribute("title", title);
				request.setAttribute("auctions", auctions);
				rd = request.getRequestDispatcher("../category.jsp");
			}
			//auctions
			else if(ps.equals("auc")){
				String seller = request.getParameter("seller");
				int code = Integer.parseInt(request.getParameter("code"));
				String begin = request.getParameter("begin");
				AuctionBean auction = dbms.getAuction(seller, code, begin);
				Vector<OfferBean> offers = dbms.getOffersByAuction(seller, code, begin);
				request.setAttribute("auction", auction);
				request.setAttribute("offers", offers);
				rd = request.getRequestDispatcher("../auction.jsp");
			}
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		}
		rd.forward(request, response);
	}

	public void doPost(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
		String ps = "";
		RequestDispatcher rd = null;
		if (request.getParameter("ps") != null) {
			ps = request.getParameter("ps");
		}

		try {
			DBMS dbms = new DBMS();
			if(ps.equals("off")){
				if(login(dbms,request.getParameter("username"),request.getParameter("password"))){
					String seller = request.getParameter("seller");
					int code = Integer.parseInt(request.getParameter("code"));
					String begin = request.getParameter("begin");
					AuctionBean auction = dbms.getLastOfferAndBasePriceOfAuction(seller,code,begin);
					request.setAttribute("auction", auction);
					rd = request.getRequestDispatcher("../offer.jsp");
				}
				else 
					rd = request.getRequestDispatcher("../loginFailed.jsp");
			}
			else if(ps.equals("voff")){
				Vector<CategoryBean> categories = dbms.getCategories();
				request.setAttribute("categories",categories);
				String username = (String)request.getParameter("username");
				String seller = (String)request.getParameter("seller");
				int code = Integer.parseInt((String)request.getParameter("code"));
				String begin = (String)request.getParameter("begin");
				int offer = Integer.parseInt((String)request.getParameter("offer"));
				dbms.insertNewOffer(username, seller, code, begin, offer);
				Vector<OfferBean> offers = dbms.getOffersByAuction(seller, code, begin);
				AuctionBean auction = dbms.getAuction(seller, code, begin);
				request.setAttribute("auction", auction);
				request.setAttribute("offers", offers);
				rd = request.getRequestDispatcher("../auction.jsp");
			}
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		}
		rd.forward(request, response);

	}
	private boolean login(DBMS dbms, String username, String password){
		String salt = dbms.getSaltByUsername(username);
		if(generateHash(password, salt).equals(dbms.getHashByUsername(username)))
			return true;

		return false;
	}

	private String generateHash(String password, String salt){
		MessageDigest md = null;
		StringBuilder hash = new StringBuilder(); 
		String text = password+salt;

		try {
			md = MessageDigest.getInstance("SHA-256");
			md.update(text.getBytes("UTF-8"));
		} catch (NoSuchAlgorithmException e) {
			e.printStackTrace();
		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
		}
		
		byte[] digest = md.digest();

		for(byte b : digest){ 
			hash.append(String.format("%02x", b&0xff)); 
		}
		return hash.toString();
	}
}
